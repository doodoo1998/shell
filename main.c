/*
* Description: <Read current direction and wirte>
*
* Author: <Linyi Fu>
*
* Date: <Oct 15 2019>
*
* Notes:
* 1. <add notes we should consider when grading>
*/

/*-------------------------Preprocessor Directives---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include "command.c"
#define _GNU_SOURCE

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>


/*--------------------------Terminal Mode------------------------------------*/
int terminal() {
    setbuf(stdout,NULL);
    /*function vars */
    char *cBuffer;
    size_t bufferSize = 32;
    size_t inputSize;
    char *command_with_arg;
    char *command_array[4], *middle;
    int i = 0;
    int stop = 1;
    int j = 0, wow = 0;
    
    
    
    
//    if(cBuffer == NULL)
//    {
//        printf("Error! Unable to allocate input buffer. \n");
//        exit(1);
//    }
    /*main run cycle*/
    while (stop) {
        /* Allocate memory to the input buffer. */

        cBuffer = (char *)malloc(bufferSize * sizeof(char));
        printf( ">>> ");
        inputSize = getline(&cBuffer, &bufferSize, stdin);
        
        char *checksemi = ";";
        for(j = 0;; j++) {
          if(cBuffer[j] == '\n') {
            cBuffer[j] = '\0';
            break;
          }
          if(cBuffer[j] == ';'){
              wow = j;
          }else{
              wow = 0;
          }
        }
        
        /* tokenize the input string */
        command_with_arg = strtok(cBuffer, ";");
        while(command_with_arg != NULL ) {
            for(int a = 0; a < 4; a++){
                command_array[a] = "";
            }
//            printf("command_with_arg:%s\n", command_with_arg);
            i = 0;
            while ((middle = strtok_r(command_with_arg," ",&command_with_arg ))){
                command_array[i] = middle;
                i += 1;
            }
//            printf("command_array: 1%s 2%s 3%s 4%s\n", command_array[0],command_array[1],command_array[2],command_array[3]);
            if (wow != 0){
                printf("Error, Unrecognized command:\n");
                break;
            }
            /*if the command is 'exit then close the program*/
            
            else if(strcmp(command_array[0], "exit\n") == 0 || strcmp(command_array[0], "exit") == 0) {
                stop = 0;
            }
            
            /*Display any commands */
            else if((strcmp(command_array[0], "ls\n") == 0)||(strcmp(command_array[0],"ls") == 0)) {
        //            printf("1%s 2%s 3%s 4%s\n", command_array[0],command_array[1],command_array[2],command_array[3]);
                if(strcmp(command_array[1], "")!= 0){
                    if((strcmp(command_array[1], "ls\n") ==0 )||(strcmp(command_array[1], "ls") ==0 )){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        
                        break;
                    }
                    else{
                        printf("Error!, Unsupported parameters for command: ls\n");
                        break;
                    }
                }
                else{
                    listDir();
                }
            }
            else if((strcmp(command_array[0], "pwd\n") == 0) || (strcmp(command_array[0],"pwd") == 0)){
                if(strcmp(command_array[1], "") != 0){
                    if((strcmp(command_array[1], "pwd\n") ==0 )||(strcmp(command_array[1], "pwd") ==0 )){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }
                    else{
                        printf("Error!, Unsupported parameters for command: pwd\n");
                        break;
                    }
                }
                else{
                    showCurrentDir();
                }
            }
            else if((strcmp(command_array[0], "mkdir\n") == 0) || (strcmp(command_array[0], "mkdir") == 0)){
//                printf("1%s 2%s 3%s 4%s\n", command_array[0],command_array[1],command_array[2],command_array[3]);
                if ((strcmp(command_array[2], "") != 0) || (strcmp(command_array[1], "") == 0)){
                    if ((strcmp(command_array[1], "mkdir\n") == 0) || (strcmp(command_array[1], "mkdir") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }
                    else{
                        printf("Error!, Unsupported parameters for command: mkdir\n");
                        break;}
                }
                else{
//                    for( j = 0;;j++){
//                        if(command_array[1][j] == '\n'){command_array[1][j] = '\0';break;}
//                    }
                    makeDir(command_array[1]);
                }

            }
            else if((strcmp(command_array[0], "cd\n") == 0) || (strcmp(command_array[0], "cd") == 0)){
                if ((strcmp(command_array[2], "") != 0) || (strcmp(command_array[1], "") == 0)){
                    if ((strcmp(command_array[1], "cd\n") == 0) || (strcmp(command_array[1], "cd") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }
                    else{
                        printf("Error!, Unsupported parameters for command: cd\n");
                    break;
                    }
                }
                
                else {
//                    for( j = 0;;j++){
//                        if(command_array[1][j] == '\n'){command_array[1][j] = '\0';break;}
                    
                    changeDir(command_array[1]);
                }
            }
            else if((strcmp(command_array[0], "cp\n") == 0) || (strcmp(command_array[0], "cp") == 0)){
                if ((strcmp(command_array[3], "") != 0) || (strcmp(command_array[2], "") == 0)){
                    if ((strcmp(command_array[1], "cp\n") == 0) || (strcmp(command_array[1], "cp") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }else{
                        printf("Error!, Unsupported parameters for command: cp\n");
                        break;
                    }
                }
                else {
//                    for(j = 0;;j++){
//                        if(command_array[2][j] == '\n'){command_array[2][j] = '\0';break;}
//                    }
//                    printf("command_array: 1%s 2%s 3%s 4%s\n", command_array[0],command_array[1],command_array[2],command_array[3]);

                    copyFile(command_array[1], command_array[2]);
                }
            }
            else if((strcmp(command_array[0], "mv\n") == 0) || (strcmp(command_array[0], "mv") == 0)){
                if ((strcmp(command_array[3], "") != 0) || (strcmp(command_array[2], "") == 0)){
                    if ((strcmp(command_array[1], "mv\n") == 0) || (strcmp(command_array[1], "mv") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }else{
                        printf("Error!, Unsupported parameters for command: mv\n");
                        break;
                    }
                }
                else {
//                    for(j = 0;;j++){
//                        if(command_array[2][j] == '\n'){command_array[2][j] = '\0';break;}
//                    }
                    moveFile(command_array[1], command_array[2]);
                }
            }
            else if((strcmp(command_array[0], "rm\n") == 0) || (strcmp(command_array[0], "rm") == 0)){
                if ((strcmp(command_array[2], "") != 0) || (strcmp(command_array[1], "") == 0)){
                    if ((strcmp(command_array[1], "rm\n") == 0) || (strcmp(command_array[1], "rm") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }else{
                        printf("Error!, Unsupported parameters for command: rm\n");
                        break;
                    }
                }
                else {
//                    for(int j = 0;;j++){
//                        if(command_array[1][j] == '\n'){command_array[1][j] = '\0';break;}
//                    }
                        deleteFile(command_array[1]);
                }
            }
            else if((strcmp(command_array[0], "cat\n") == 0) || (strcmp(command_array[0], "cat") == 0)){
                if ((strcmp(command_array[2], "") != 0) || (strcmp(command_array[1], "") == 0)){
                    if ((strcmp(command_array[1], "cat\n") == 0) || (strcmp(command_array[1], "cat") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }else{
                        printf("Error!, Unsupported parameters for command: cat\n");
                        break;
                    }
                }
                else {
//                    for(int j = 0;;j++){
//                        if(command_array[1][j] == '\n'){command_array[1][j] = '\0';break;}
//                    }
                    displayFile(command_array[1]);
                }
            }
            
            

            else {printf("Error: Unrecognized command: %s\n" , command_array[0]); break;}
            command_with_arg = strtok(NULL, ";");
        }
//        if(command_with_arg != NULL) {if(strcmp(command_with_arg, "exit\n") == 0 || strcmp(command_with_arg, "exit") == 0) { break; }}
        free(cBuffer);

    }
    /*Free the allocated memory*/
    return 0;
}
/*---------------------------File Read Mode------------------------------------------------------------*/

int file_read(char *file){
    /*function vars */
    char *cBuffer;
    size_t bufferSize = 32;
    size_t inputSize;
    char *command_with_arg;
    char *command_array[4], *middle;
    int i = 0;
    int stop = 1;
    FILE *fp = fopen(file, "r");
    FILE *output_file;
    int check = 0, j = 0;

    
    
    output_file = freopen("output.txt", "w", stdout);
    // main loop
    /* Allocate memory to the input buffer. */
    cBuffer = (char *)malloc(bufferSize * sizeof(char));
        /*main run cycle*/
    while (((inputSize = getline(&cBuffer,&bufferSize,fp)) != -1)) {
        check = 0;
        for(j = 0;; j++) {
          if(cBuffer[j] == '\n') {
              
            cBuffer[j] = '\0';
            break;
          }
          if(cBuffer[j] == ';'){check = j;}
          else{check = 0;}
        }
        /* tokenize the input string */
        command_with_arg = strtok(cBuffer, ";");
        while(command_with_arg != NULL ) {
            for(int a = 0; a < 4; a++){
                command_array[a] = "";
            }
//            printf("command_with_arg:%s\n", command_with_arg);
            i = 0;
            while ((middle = strtok_r(command_with_arg," ",&command_with_arg ))){
                command_array[i] = middle;
                i += 1;
            }
            if (check != 0){
                printf("Error, Unrecognized command:\n");
                break;
            }

            
            else if((strcmp(command_array[0], "ls\n") == 0)||(strcmp(command_array[0],"ls") == 0)) {
                if(strcmp(command_array[1], "")!= 0){
                    if((strcmp(command_array[1], "ls\n") ==0 )||(strcmp(command_array[1], "ls") ==0 )){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;}
                    else{
                        printf("Error!, Unsupported parameters for command: ls\n");
                        break;}
                }
                else{
                    listDir();
                }
            }
            else if((strcmp(command_array[0], "pwd\n") == 0) || (strcmp(command_array[0],"pwd") == 0)){
                if(strcmp(command_array[1], "") != 0){
                    if((strcmp(command_array[1], "pwd\n") ==0 )||(strcmp(command_array[1], "pwd") ==0 )){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }
                    else{
                        printf("Error!, Unsupported parameters for command: pwd\n");
                        break;
                    }
                }
                else{
                    showCurrentDir();

                }
            }
            else if((strcmp(command_array[0], "mkdir\n") == 0) || (strcmp(command_array[0], "mkdir") == 0)){
    //                printf("1%s 2%s 3%s 4%s\n", command_array[0],command_array[1],command_array[2],command_array[3]);
                if ((strcmp(command_array[2], "") != 0) || (strcmp(command_array[1], "") == 0)){
                    if ((strcmp(command_array[1], "mkdir\n") == 0) || (strcmp(command_array[1], "mkdir") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }
                    else{
                        printf("Error!, Unsupported parameters for command: mkdir\n");
                        break;}
                }
                else{
//                    for(int j = 0;;j++){
//                        if(command_array[1][j] == '\n'){command_array[1][j] = '\0';break;}
//                    }
                    makeDir(command_array[1]);
                }

            }
            else if((strcmp(command_array[0], "cd\n") == 0) || (strcmp(command_array[0], "cd") == 0)){
                if ((strcmp(command_array[2], "") != 0) || (strcmp(command_array[1], "") == 0)){
                    if ((strcmp(command_array[1], "cd\n") == 0) || (strcmp(command_array[1], "cd") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }
                    else
                    printf("Error!, Unsupported parameters for command: cd\n");
                    break;}
                
                else {
//                    for(int j = 0;;j++){
//                        if(command_array[1][j] == '\n'){command_array[1][j] = '\0';break;}
//                    }
                    changeDir(command_array[1]);
                }
            }
            else if((strcmp(command_array[0], "cp\n") == 0) || (strcmp(command_array[0], "cp") == 0)){
                if ((strcmp(command_array[3], "") != 0) || (strcmp(command_array[2], "") == 0)){
                    if ((strcmp(command_array[1], "cp\n") == 0) || (strcmp(command_array[1], "cp") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }else{
                        printf("Error!, Unsupported parameters for command: cp\n");
                        break;
                    }
                }
                else {
//                    for(int j = 0;;j++){
//                        if(command_array[2][j] == '\n'){command_array[2][j] = '\0';break;}
//                    }
                    copyFile(command_array[1], command_array[2]);
                }
            }
            else if((strcmp(command_array[0], "mv\n") == 0) || (strcmp(command_array[0], "mv") == 0)){
                if ((strcmp(command_array[3], "") != 0) || (strcmp(command_array[2], "") == 0)){
                    if ((strcmp(command_array[1], "mv\n") == 0) || (strcmp(command_array[1], "mv") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }else{
                        printf("Error!, Unsupported parameters for command: mv\n");
                        break;
                    }
                }
                else {
//                    for(int j = 0;;j++){
//                        if(command_array[2][j] == '\n'){command_array[2][j] = '\0';break;}
//                    }
                    moveFile(command_array[1], command_array[2]);
                }
            }
            else if((strcmp(command_array[0], "rm\n") == 0) || (strcmp(command_array[0], "rm") == 0)){
                if ((strcmp(command_array[2], "") != 0) || (strcmp(command_array[1], "") == 0)){
                    if ((strcmp(command_array[1], "rm\n") == 0) || (strcmp(command_array[1], "rm") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }else{
                        printf("Error!, Unsupported parameters for command: rm\n");
                        break;
                    }
                }
                else {
//                    for(int j = 0;;j++){
//                        if(command_array[1][j] == '\n'){command_array[1][j] = '\0';break;}
//                    }
                    deleteFile(command_array[1]);
                }
            }
            else if((strcmp(command_array[0], "cat\n") == 0) || (strcmp(command_array[0], "cat") == 0)){
                if ((strcmp(command_array[2], "") != 0) || (strcmp(command_array[1], "") == 0)){
                    if ((strcmp(command_array[1], "cat\n") == 0) || (strcmp(command_array[1], "cat") == 0)){
                        printf("Error! Incorrect syntax. No control code found!\n");
                        break;
                    }else{
                        printf("Error!, Unsupported parameters for command: cat\n");
                        break;
                    }
                }
                else {
//                    for(int j = 0;;j++){
//                        if(command_array[1][j] == '\n'){command_array[1][j] = '\0';break;}
//                    }
                    displayFile(command_array[1]);
                }
            }
            else {
                printf("Error: Unrecognized command: %s\n" , command_array[0]);
                break;
            }
        command_with_arg = strtok(NULL, ";");
        }
//        if(command_with_arg != NULL) {if(strcmp(command_with_arg, "exit\n") == 0 || strcmp(command_with_arg, "exit") == 0) { break; }}
            
        }
    free(cBuffer);
    fclose(fp);
    fclose(output_file);
    return 0;
}
/*---------------------------Main Function-------------------------------------*/
int main(int argc, char *argv[]){
    setbuf(stdout, NULL);
    if(strcmp(argv[1], "-command") == 0){
        terminal();
    }
    else if(strcmp(argv[1], "-f")==0){
        file_read(argv[2]);
    }
    else{printf("error, please input correct command.");}
    return 0;
}
/*-----------------------------Program End-----------------------------------*/

