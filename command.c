/*
* Description: <Read current direction and wirte>
*
* Author: <Linyi Fu>
*
* Date: <Oct 15 2019>
*
* Notes:
* 1. <add notes we should consider when grading>
*/

/*-------------------------Preprocessor Directives---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include "command.h"
#define _GNU_SOURCE

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

/*------------------------Function definition---------------------------------*/
void listDir(){
    struct dirent *dp;
    char cwd[256];
    
    getcwd(cwd,sizeof(cwd));
    DIR *dr = opendir(cwd);
    
    while ((dp = readdir(dr)) != NULL){
        write(STDOUT_FILENO, dp -> d_name, strlen(dp -> d_name) );
        write(STDOUT_FILENO, "\n" , 1);
    }
    closedir(dr);
}

void showCurrentDir(){
    char currend_dir[256];
    if(getcwd(currend_dir, sizeof(currend_dir)) != NULL){
        write(STDOUT_FILENO, currend_dir, strlen(currend_dir) );
        write(STDOUT_FILENO, "\n" , 1);

    }
}

void makeDir(char *dirName){
    mkdir(dirName, S_IRWXU);
}

void changeDir(char *dirName){
    chdir(dirName);
}

void copyFile(char *sourcePath, char *destinationPath){
    int destination_move, source_move, fm;
    char *dest_with_filename , current_dir[256];
    getcwd(current_dir,sizeof(current_dir));
    
    source_move = open (sourcePath, O_RDONLY);
    
    
    char *buf = (char*)calloc(1024, sizeof(char));
    fm = read(source_move,buf,128*1024);
    chdir(destinationPath);
    dest_with_filename = strrchr(sourcePath, '/');
    destination_move = open(dest_with_filename+1, O_WRONLY |O_TRUNC | O_CREAT,777);
    
    destination_move = write(destination_move,buf,128*1024);
    

    close(destination_move);
    close(source_move);
    chdir(current_dir);
    free(buf);
}

void moveFile(char *sourcePath, char *destinationPath){
    int destination_move, source_move, index, fm;
    char current_dir[256];
    getcwd(current_dir,sizeof(current_dir));

    destination_move = open(destinationPath, O_WRONLY |O_TRUNC | O_CREAT,777);
    source_move = open (sourcePath, O_RDONLY);
    char *buf = (char*)calloc(1024, sizeof(char));
    fm = read(source_move,buf,128*1024);
    unlink(sourcePath);
    chdir(destinationPath);
    destination_move = write(destination_move,buf,128*1024);
    chdir(current_dir);
    close(destination_move);
    close(source_move);
    free(buf);
    
    
}

void deleteFile(char *filename){
    unlink(filename);
}

void displayFile(char *filename){
    int sz , fd;
    char *buf;
    fd = open(filename, O_RDONLY);
    buf = (char *)calloc(1024,sizeof(char));
    if (fd == -1){
        perror("DF open error");
        exit(1);
    }
    sz = read(fd,buf,128*1024);
    if (sz == -1 ){
        perror("DF read error");
        exit(1);
    }
    write(STDOUT_FILENO, buf, strlen(buf));
    close(fd);
    free(buf);
}








